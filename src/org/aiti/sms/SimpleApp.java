package org.aiti.sms;

import java.util.ArrayList;
import java.util.Vector;

import org.smslib.*;
import org.smslib.Message.MessageTypes;

/**
 * An example SMS message handler that is called by the AITI SMS System.
 * Note that for each message received, a separate thread is spawned to handle 
 * the message.  This thread creates a new object of the registered handler 
 * (in this case a new SimpleApp object) to service the inbound message. 
 * 
 * This service sends the text of the message back to the user.
 * 
 * @author AITI
 *
 */
public class SimpleApp implements IAITIInboundMessageNotification {
	/**
	 * This method is called by the AITI-SMS runtime library when a message is received.
	 * Each message creates a new SimpleApp object to service the message.
	 * 
	 * @param srv The service we received from, can be used to send a response
	 * @param gatewayId The id of the gateway that received the message
	 * @param msgType The type of the messsage, usually or.smslib.MessageTypes.INBOUND
	 * @param msg The The inbound message received
	 */
	
	String outboundMsg = "";
	ArrayList<String> outboundResponses;
	Vector<String> details;
	public void process(Service srv, String gatewayId, MessageTypes msgType, InboundMessage msg) {
		try {
			
			//inbound message handler
			inboundMessageProcessor(msg.getText(), msg.getOriginator());
			
			//for viewing purposes
			System.err.println("oubound responses count : " + outboundResponses.size());
			
			//send message(s) depending on the size of the outbound responses
			for (int i = 0; i < outboundResponses.size(); i++) {
				//for viewing purposes
				System.err.println("Outbound " + (i+1) +":"+ outboundResponses.get(i));
				
				//send the outbound message
				srv.sendMessage(new OutboundMessage(msg.getOriginator(), outboundResponses.get(i)));
			}	
			
		} catch (Exception e) {
			System.err.println("Error sending message");
			e.printStackTrace();
		}
	}
	
	public void inboundMessageProcessor(String inboundMessage, String msgOriginator) {
		outboundResponses = new ArrayList<String>();
		
		if (inboundMessage.matches("(\\s+)?[pP][eE][nN][zZ][iI](\\s+)?")) {
			
			//if message is 'penzi'
			outboundResponses.add(serviceActivationResponse());
			
		} else if (inboundMessage.matches("(\\s+)?[nN][eE][xX][tT](\\s+)?")) {
			
			//if messasge is 'next'
			String [] details = {"Lina", "29", "0722123456", "Dorrine", "26", "0722123456", "Maureen", "22", "0712345678"};
			outboundResponses.add(matchingRequestResponse(2, details));
			
		} else if (inboundMessage.matches("(\\s+)?[sS][tT][aA][rR][tT]#[A-za-z]+#(1[8-9]|[2-5][0-9])#([mM]|[fF]|[mM][aA][lL][eE]|[fF][eE][mM][aA][lL][eE])#[A-Za-z]+#[A-Za-z]+(\\s+)?")) {
			
			//if message starts with 'start'
			outboundResponses.add(serviceRegistrationResponse());
			
		} else if (inboundMessage.matches("(\\s+)?[dD][eE][tT][aA][iI][lL][sS]#[A-za-z]+#[A-za-z]+#[A-za-z]+#[A-za-z]+#[A-za-z]+(\\s+)?")) {
			
			//if message starts with 'details'
			outboundResponses.add(detailsRegistrationResponse());
			
		} else if (inboundMessage.matches("(\\s+)?[mM][yY][sS][eE][lL][fF]\\D+(\\s+)?")) {
			
			//if message starts with 'myself'
			outboundResponses.add(selfDescriptionResponse());
			
		} else if (inboundMessage.matches("(\\s+)?[mM][aA][tT][cC][hH]#(1[8-9]|[2-5][0-9])-?(1[8-9]|[2-5][0-9])?#[A-Za-z]+(\\s+)?")) {
			
			//if message starts with 'match'
			String [] details = {"30", "ladies", "lady", "her"};
			
			outboundResponses.add(matchingRequestResponse(1, details));
			
			outboundMsg = "";
			String [] details2 = {"Lina", "29", "0722123456", "Dorrine", "26", "0722123456", "Maureen", "22", "0712345678"};
			outboundResponses.add(matchingRequestResponse(2, details2));
			
		} else if (inboundMessage.matches("(\\s+)?07[0-9]{8}(\\s+)?")) {
			
			//if message is a kenyan phone number
			String [] details = {"Lina", "29", "coast", "lamu", "graduate", "accountant", "married", "christian", "kikuyu"};
			outboundResponses.add(moreDetailsResponse(inboundMessage, details));
			
		} else if (inboundMessage.matches("(\\s+)?[dD][eE][sS][cC][rR][iI][bB][eE]\\s+07[0-9]{8}(\\s+)?")) {
			
			//if message starts with 'describe'
			details = new Vector<String>();
			outboundResponses.add(selfDesciptionResponse("Diligent, loving, dedicated and will make you tick"));
			
			if (Main.describleHolder.get(msgOriginator) == null) {
				
				//first time sending message that starts with 'describe'
				details.add("Andrew");
				details.add("26-30");
				details.add("Kisumu");
				details.add("4");
				
				//append notice 1 response to the outbound response data structure
				outboundResponses.add(noticeResponse(1, details));
				Main.describleHolder.put(msgOriginator, 1);
			} else if (Main.describleHolder.get(msgOriginator) == 1) {
				
				//second time sending message that starts with 'describe'
				details.add("Andrew");
				details.add("Tuesday");
				details.add("25");
				//append notice 3 response to the outbound response data structure
				outboundResponses.add(noticeResponse(3, details));
				
				//append deactivation response to the outbound response data structure
				outboundResponses.add(deactivationRespnse());
				Main.describleHolder.remove(msgOriginator);
			}
			
		} else if (inboundMessage.matches("(\\s+)?[aA][cC][tT][iI][vV][aA][tT][eE](\\s+)?")) {
			
			//if message starts with 'activate'
			outboundResponses.add(activationResponse());
			
		} else if (inboundMessage.matches("(\\s+)?[yY][eE][sS](\\s+)?")) {
			
			//if message starts with 'yes'
			outboundMsg = "";
			if (Main.yesHolder.get(msgOriginator) == null) {
				
				//if msg originator initially doesn't exist in holder data structure
				String [] userMatches = {"Lina", "29", "0722123456", "Dorrine", "26", "0722123456", "Maureen", "22", "0712345678"};
				outboundResponses.add(matchingRequestResponse(2, userMatches));
				
				details = new Vector<String>();
				details.add("Andrew");
				details.add("lady");
				details.add("Lina");
				details.add("25");
				details.add("Kisumu");
				details.add("her");
				outboundResponses.add(noticeResponse(2, details));
				
				Main.yesHolder.put(msgOriginator, 1);
			} else {
				Main.yesHolder.remove(msgOriginator);
				String [] matches = {"Lina", "29", "coast", "lamu", "graduate", "accountant", "married", "christian", "kikuyu"};
				outboundResponses.add(moreDetailsResponse(msgOriginator, matches));
			}
		} else {
			
			//if the message sent does not match any of regex expressions
			outboundResponses.add("problem occured while processing your request. kindly check you message format :-(");
		}
	}
	
	public String serviceActivationResponse() {
		outboundMsg = "Welcome to our dating service with 6000 potential dating partners! To register sms "
				+ "start#name#age#sex#province#town to 5001 E.G start#Mike#25#Male#coast#malindi";
		return outboundMsg; 
	}
	
	public String serviceRegistrationResponse() {
		outboundMsg = "Thank you. SMS details#level of education#profession#marital status#religion#tribe to"
				+ "5001 E.G details#diploma#accountant#single#christian#mijikenda";
		return outboundMsg;
	}
	
	public String promptIfNoReply() {
		outboundMsg = "You were registered for dating with your initial details. Enjoy yourself. To search "
				+ "for a MPENZI, SMS Match#age#town to 5001 E.G Match#23-25#nairobi";
		return outboundMsg;
	}
	
	public String detailsRegistrationResponse() {
		outboundMsg = "This is the last stage of registration. SMS a brief description of yourself to 5001"
				+ "starting with the word MYSELF .E.G MYSELF chocolate, lovely, sexy etc";
		return outboundMsg;
	}
	
	public String selfDescriptionResponse() {
		outboundMsg = "You are now registered! Enjoy yourself. To search fo a MPENZI, SMS Match#age#town to"
				+ "5001. E.G Match#23-25#Nairobi";
		return outboundMsg;
	}

	public String matchingRequestResponse(int responseNum, String [] details) {
		String ending = "";
		if (responseNum == 1) {
			/**
			 * {@details[0]} <number>
			 * {@details[1]} <"ladies"|"gents">
			 * {@details[2]} <"lady"|"gent">
			 * {@details[3]} <"her"|"him">
			 */
			outboundMsg = "We have "+details[0]+" "+details[1]+" who match your choice! We will send you details of 3 of them shortly."
					+ "To get more details about a "+details[2]+", SMS "+details[3]+" number E.G 0722123456 to 5001";
			return outboundMsg;
		}
		
		/**
		 * details array format: [name, age, phonenumber, name, age, phonenumber ...]
		 */
		for (int i = 0; i < details.length; i+=3) {
			ending = (i+2 == details.length - 1) ? "." : ", ";
			outboundMsg += details[i] + ", aged " + details[i+1] + ", " + details[i+2] + ending;
		}
		// TODO: change the value 27 to be dynamic
		outboundMsg += " Send NEXT to 5001 to receive details of the remaining 27 ladies";
		return outboundMsg;
	}
	
	public String moreDetailsResponse(String phoneNumber,String [] details) {
		for (int i = 0; i < details.length; i++) {
			if (i == 1) {
				outboundMsg += "aged " + details[i] + ", ";
			} else if (i == 2) {
				outboundMsg += details[i] + " province, ";
			} else if (i == 3) {
				outboundMsg += details[i] + " town, ";
			} else if (i == details.length - 1) {
				outboundMsg += details[i] + ". ";
			} else {
				outboundMsg += details[i] + ", ";
			}
		}
		
		outboundMsg += "Send DESCRIBE " + phoneNumber + " to get more details about "+details[0] ;
		return outboundMsg;
	}
	
	public String selfDesciptionResponse(String moreDetails) {
		outboundMsg = moreDetails;
		return outboundMsg;
	}
	
	public String noticeResponse(int noticeNum, Vector <String> details) {
		outboundMsg = "";
		if (noticeNum == 1) {
			outboundMsg = "Hi " + details.elementAt(0) + ", Last time you wanted a Date of age " + details.elementAt(1) + " based"
					+ "in " + details.elementAt(2) + ". We have " + details.elementAt(3) + " new members matching your choice. Are "
							+ "you intereseted? Send YES to 5001";
		} else if (noticeNum == 2) {
			outboundMsg = "Hi " + details.elementAt(0) + ", a " + details.elementAt(1) + " called " + details.elementAt(2) + " is "
					+ "interested in u and requested ur details. Aged " + details.elementAt(3) + ", based in " + details.elementAt(4) + ". "
							+ "Do you want to know more about " + details.elementAt(5) + " ? Send YES to 5001";
		} else if (noticeNum == 3) { 
			outboundMsg = "Hi " + details.elementAt(0) + "! You are due for de-activation from our dating service on " + details.elementAt(1) + " "
					+ "the " + details.elementAt(2) + ". To re-activate send ACTIVATE to 5001";
		}
		return outboundMsg;
	}
	
	public String activationResponse() {
		outboundMsg = "You are now regisered! Enjoy yourself. To search for a MPENZI, "
				+ "SMS Match#age#town to 5001. E.G March#23-25#Nairobi";
		return outboundMsg;
	}
	
	public String deactivationRespnse() {
		outboundMsg = "DEACTIVATION for those who have been inactive for more than a month";
		return outboundMsg;
	}
}

